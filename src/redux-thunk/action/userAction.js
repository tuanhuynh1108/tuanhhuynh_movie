import { userLogin } from "../../service/userServ";
import { USER_LOGIN_REDUX } from "../reducer/constant/userConstant";

export const userActionThunk = (formLogin, handleSucess) => {
  return (dispath) => {
    userLogin
      .postLogin(formLogin)
      .then((res) => {
        console.log(res);
        dispath({
          type: USER_LOGIN_REDUX,
          payload: res.data.content,
        });
        handleSucess(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
