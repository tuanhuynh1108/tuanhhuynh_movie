import { applyMiddleware, combineReducers, createStore } from "redux";
import thunk from "redux-thunk";
import { userReducer } from "./userReducer";
export const rootReducer = combineReducers({
  // state ứng dụng
  userReducer,
});
