import { userLocal } from "../../../page/localStorage/localStorage";
import { USER_LOGIN_REDUX } from "../constant/userConstant";

const initialState = {
  userInfo: userLocal.get(),
  // userInfo: null,
};

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN_REDUX:
      return { ...state, userInfo: payload };

    default:
      return state;
  }
};
export default userReducer;
