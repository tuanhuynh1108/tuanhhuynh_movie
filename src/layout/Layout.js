import React from "react";
import Header from "../page/Header/Header";
import Footer from "../page/Footer/Footer";

export default function Layout({ Component }) {
  return (
    <div>
      <Header />
      <Footer />
      <Component />
    </div>
  );
}
// tái sử dụng header và footer
