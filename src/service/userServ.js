import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const userLogin = {
  //   postLogin: (formLogin) => {
  //     return axios.post(
  //       `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
  //       {
  //         data: formLogin,
  //       },
  //       { headers: configHeaders() }
  //     );
  //   },
  postLogin: (formLogin) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: formLogin,
      headers: configHeaders(),
    });
  },
};
