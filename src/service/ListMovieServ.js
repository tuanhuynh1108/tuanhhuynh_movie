import axios from "axios";
import { BASE_URL, configHeaders } from "./config";

export const movieServ = {
  getMovieList: () => {
    return axios.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03`, {
      headers: configHeaders(),
    });
  },
  getMovieByTheater: () => {
    return axios.get(
      `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01`,
      {
        headers: configHeaders(),
      }
    );
  },
  getMovieDetail: (id) => {
    return axios.get(
      `${BASE_URL}/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,

      { headers: configHeaders() }
    );
  },
};
