import React, { useEffect } from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userLogin } from "../../service/userServ";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { userLocal } from "../localStorage/localStorage";
import { USER_LOGIN_REDUX } from "../../redux-thunk/reducer/constant/userConstant";
import { userActionThunk } from "../../redux-thunk/action/userAction";

export default function LoginPage() {
  let navigate = useNavigate();
  let ditpasth = useDispatch();
  const onFinishThunk = (values) => {
    // console.log("Success:", values);
    let handleSucess = () => {
      message.success("Đăng Nhập Thành Công");
      navigate("/");
      userLocal.get();
    };
    ditpasth(userActionThunk(values, handleSucess));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="flex h-screen w-screen justify-center items-center bg-slate-700 ">
      <div className="bg-slate-100 rounded w-11/12 flex p-10">
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinishThunk}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          {/* <Form.Item
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item> */}

          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Đăng Nhập
            </Button>
            <br />
            <br />
            <NavLink to="/register">
              <a href="">Đăng ký</a>
            </NavLink>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}
