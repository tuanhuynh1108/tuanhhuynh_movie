import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function CartMovie({ movieList }) {
  return (
    <div>
      <Card
        hoverable
        style={{
          width: 250,
        }}
        cover={
          <img
            alt="example"
            src={movieList.hinhAnh}
            style={{ height: 350, objectFit: "cover", objectPosition: "top" }}
          />
        }
      >
        <Meta
          title={movieList.tenPhim}
          //   xem chi tiết
        />
        <NavLink to={`/detail/${movieList.maPhim}`}>
          <button className="hover:bg-red-500 bg-slate-300 text-white rounded px-2 py-1 mt-3">
            Xem Chi Tiết
          </button>
        </NavLink>
      </Card>
    </div>
  );
}
