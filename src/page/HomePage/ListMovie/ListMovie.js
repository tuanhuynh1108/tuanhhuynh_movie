import React, { useEffect, useState } from "react";
import CartMovie from "./CartMovie";
import { movieServ } from "../../../service/ListMovieServ";

export default function ListMovie() {
  let [movieList, setMovieList] = useState();
  useEffect(() => {
    movieServ
      .getMovieList()
      .then((res) => {
        console.log(res.data.content);
        setMovieList(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="container grid grid-cols-4 gap-4 mx-auto ">
      {movieList?.map((item) => {
        return <CartMovie movieList={item} />;
      })}
    </div>
  );
}
