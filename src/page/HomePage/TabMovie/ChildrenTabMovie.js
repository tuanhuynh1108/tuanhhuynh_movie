import moment from "moment/moment";
import React from "react";

export default function ChildrenTabMovie({ danhSachPhim }) {
  return (
    <div style={{ height: 600 }} className="overflow-auto">
      {danhSachPhim.map((danhSach) => {
        return (
          <div className="flex">
            <img
              src={danhSach.hinhAnh}
              alt="#"
              style={{ height: 250, objectFit: "cover", objectPosition: "top" }}
              className="w-36"
            />
            <div>
              <p>{danhSach.tenPhim}</p>
              <div>
                {danhSach.lstLichChieuTheoPhim.map((lich) => {
                  return <p>{moment(lich.ngayChieuGioChieu).format("l")}</p>;
                })}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}
