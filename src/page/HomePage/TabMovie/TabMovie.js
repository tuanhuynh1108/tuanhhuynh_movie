import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieServ } from "../../../service/ListMovieServ";
import ChildrenTabMovie from "./ChildrenTabMovie";
export default function TabMovie() {
  const onChange = (key) => {
    // console.log(key);
  };
  const [dataMovie, setDataMovie] = useState([]);

  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        console.log(res.data.content);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderTabMovie = () => {
    return dataMovie.map((heThongRap, index) => {
      return {
        key: index,
        label: <img src={heThongRap.logo} alt="#" className="w-16" />,
        children: (
          <Tabs
            style={{ height: 600 }}
            defaultActiveKey="1"
            onChange={onChange}
            tabPosition="left"
            items={heThongRap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.diaChi,
                label: cumRap.tenCumRap,
                children: (
                  <ChildrenTabMovie danhSachPhim={cumRap.danhSachPhim} />
                ),
              };
            })}
          />
        ),
      };
    });
  };
  return (
    <div className="container mt-10">
      <Tabs
        style={{ height: 600 }}
        defaultActiveKey="1"
        items={renderTabMovie()}
        onChange={onChange}
        tabPosition="left"
      />
    </div>
  );
}
