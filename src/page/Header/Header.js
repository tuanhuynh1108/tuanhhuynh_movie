import { faUserCircle } from "@fortawesome/free-regular-svg-icons";
import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { userLocal } from "../localStorage/localStorage";

export default function Header() {
  // lấy dữ liệu từ redux - userSelector
  let userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });
  console.log("userInfo: ", userInfo);
  let handleLogout = () => {
    userLocal.remove();
    window.location.href = "/";
  };
  let renderUser = () => {
    if (userInfo) {
      return (
        <div className="flex justify-end items-center">
          <span className="bg-transparent hover:bg-slate-900 text-slate-500 font-semibold hover:text-white py-2 px-4  rounded mr-1 ">
            {userInfo.hoTen}
          </span>
          <button
            className="bg-transparent hover:bg-slate-900 text-slate-500 font-semibold hover:text-white py-2 px-4    rounded mr-1 "
            onClick={handleLogout}
          >
            Đăng Xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="flex justify-end items-center">
          <NavLink to="/login">
            <button class="bg-transparent hover:bg-slate-900 text-slate-500 font-semibold hover:text-white py-2 px-4    rounded mr-1 ">
              <FontAwesomeIcon
                icon={faUserCircle}
                className="mr-5 text-slate-500"
                size="lg"
              />
              Đăng Nhập
            </button>
          </NavLink>

          <NavLink to="/register">
            <button class="bg-transparent rounded hover:bg-slate-900 text-slate-500 font-semibold hover:text-white py-2 px-4   ">
              <FontAwesomeIcon
                icon={faUserCircle}
                className="mr-5 text-slate-500"
              />
              Đăng ký
            </button>
          </NavLink>
        </div>
      );
    }
  };
  return (
    <div className="flex container  py-5 justify-between items-center">
      <div className="flex">
        <img
          src="http://demo1.cybersoft.edu.vn/logo.png"
          alt="#"
          style={{
            width: 250,
            height: 100,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat  ",
          }}
        />
      </div>
      <div>
        <ul className="flex justify-between mr-10 font-medium ">
          <li className="mr-10">Lịch Chiếu</li>
          <li className="mr-10">Cụm Rạp</li>
          <li className="mr-10">Tin Tức</li>
          <li className="mr-10">Ứng Dụng</li>
        </ul>
      </div>
      <div>{renderUser()}</div>
    </div>
  );
}
