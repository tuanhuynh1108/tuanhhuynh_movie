export const userLocal = {
  get: () => {
    let JSONdata = localStorage.getItem("USER_LOGIN");
    return JSON.parse(JSONdata);
  },
  set: (value) => {
    let dataJSON = JSON.stringify(value);
    localStorage.setItem("USER_LOGIN", dataJSON);
  },
  remove: () => {
    localStorage.removeItem("USER_LOGIN");
  },
};
