import React, { useEffect, useState } from "react";
import { movieServ } from "../../service/ListMovieServ";
import { useParams } from "react-router-dom";
import { Card } from "antd";
import { Progress, Space } from "antd";
const { Meta } = Card;
export default function DetailPage() {
  let params = useParams();
  const [detail, setDetail] = useState({});
  useEffect(() => {
    movieServ
      .getMovieDetail(params.id)
      .then((res) => {
        console.log(res);
        setDetail(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div className="container flex justify-between items-center space-x-10">
      <Card
        hoverable
        style={{
          width: 240,
        }}
        cover={<img alt="example" src={detail.hinhAnh} />}
      >
        <Meta title={detail.tenPhim} description="" />
      </Card>
      <Progress
        type="circle"
        percent={detail.danhGia * 10}
        status="exception"
        format={(percent) => {
          return percent / 10 + " Điểm";
        }}
        size={250}
      />
      <div>
        <h2 className="font-medium hover:tracking-wider text-3xl duration-150">
          {detail.tenPhim}
        </h2>
        <p className="mt-5 text-zinc-500">{detail.moTa}</p>
      </div>
    </div>
  );
}
